<?php

	$sums = array();
	/**
	 * i originally assumed we only wanted numbers with 5 digits, as the example only included numbers
	 * with 4 digits. running that did not work, so i tried any number < 6 digits... which also did not work
	 * i'm not sure how you're suposed to know 194,979 is the last, but adding a bunch of 0's to the
	 * end of my loop found me the right answer.
	 */
	for ( $i = 2; $i < 10000000; $i++ ) {

		$digits = str_split( $i );

		$digits = array_map( function ( $value ) {
			return pow( $value, 5 );
		}, $digits );

		$sum = array_sum( $digits );

		if ( $sum == $i ) {
			$sums[] = $i;
		}

	}

	print_r( $sums );

	var_dump( array_sum( $sums ) );

?>