<?php

	$fibonacci = array( 1, 2 );

	// prime our first 2 terms
	$term1 = 1;
	$term2 = 2;

	do {

		$new_term = $term1 + $term2;

		$fibonacci[] = $new_term;

		// everything bumps down a level
		$term1 = $term2;
		$term2 = $new_term;

	}
	while ( $new_term < 4000000 && $term1 < 4000000 && $term2 < 4000000 );

	print_r($fibonacci);

	$evens = array_filter( $fibonacci, function ( $var ) {
		return ( $var % 2 == 0 );
	} );

	print_r($evens);

	var_dump( array_sum( $evens ) );

?>