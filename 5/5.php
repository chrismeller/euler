<?php

	$i = 1;

	do {

		$all = true;
		for ( $j = 1; $j <= 20; $j++ ) {

			if ( $i % $j != 0 ) {
				$all = false;
				break;
			}

		}

		if ( $i % 10 == 0 ) {
			echo $i . "\n";
		}

		$i++;

	}
	while ( $all == false );

	echo --$i . ' is divisible by all numbers.' . "\n";

?>