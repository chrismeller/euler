<?php

	$number = 600851475143;

	$factors = factor( $number );

	$primes = array();
	foreach ( $factors as $factor ) {
		if ( is_prime( $factor ) ) {
			$primes[] = $factor;
		}
	}

	echo 'Largest factor: ' . max( $factors ) . "\n";
	echo 'Largest prime factor: ' . max( $primes ) . "\n";

	function factor ( $number ) {

		$factors = array();
		for ( $i = round( sqrt( $number ) ); $i > 0; $i-- ) {

			if ( $number % $i == 0 ) {
				$factors[] = $i;
				$factors[] = $number / $i;
			}

		}

		rsort( $factors );

		return $factors;

	}

	function is_prime ( $number ) {

		$factors = factor( $number );

		$keyed = array_combine( $factors, $factors );

		unset( $keyed[ $number ] );
		unset( $keyed[ 1 ] );

		if ( count( $keyed ) > 0 ) {
			return false;
		}
		else {
			return true;
		}

	}

?>