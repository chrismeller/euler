<?php

	// prime our first 2 terms
	$term1 = 1;
	$term2 = 1;

	// we've primed 2 already, so start with 3
	$i = 3;
	do {

		$new_term = bcadd( $term1, $term2 );

		//echo $i . ' ' . $term1 . ' + ' . $term2 . ' = ' . $new_term . "\n";

		// everything bumps down a level
		$term1 = $term2;
		$term2 = $new_term;

		$i++;

	}
	while ( bcsub( $new_term, bcpow( 10, 999 ) ) <= 0 );

	var_dump( --$i, $new_term );

?>