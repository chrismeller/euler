<?php

	require('common.php');

	$goal_number_of_divisors = 500;

	$i = 1;
	do {

		$triangle = calculate_triangle( $i );

		$factors = factor( $triangle );

		if ( $i % 100 == 0 ) {
			echo $i . ': ' . count( $factors ) . ' divisors' . "\n";
		}

		$i++;

	}
	while ( count( $factors ) <= $goal_number_of_divisors );

	echo 'First triangle with over ' . $goal_number_of_divisors . ' divisors: ' . $triangle . ' ' . count( $factors ) . "\n";

	function calculate_triangle ( $nth ) {

		$sum = 0;
		for ( $i = 1; $i <= $nth; $i++ ) {
			$sum = $sum + $i;
		}

		return $sum;

	}

?>