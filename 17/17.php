<?php
	
	date_default_timezone_set('America/New_York');
	ini_set('display_errors', true);
	error_reporting(-1);
	
	$ones = array(
		'one',
		'two',
		'three',
		'four',
		'five',
		'six',
		'seven',
		'eight',
		'nine',
	);
	
	$teens = array(
		'ten',
		'eleven',
		'twelve',
		'thirteen',
		'fourteen',
		'fifteen',
		'sixteen',
		'seventeen',
		'eighteen',
		'nineteen',
	);
	
	$tens = array(
		'twenty',
		'thirty',
		'forty',
		'fifty',
		'sixty',
		'seventy',
		'eighty',
		'ninety',
	);
	
	$numbers = array_merge( $ones, $teens );
	foreach ( $tens as $ten ) {
		
		$numbers[] = $ten;
		
		foreach ( $ones as $one ) {
			$numbers[] = $ten . '-' . $one;
		}
		
	}
	
	foreach ( $ones as $hundred ) {
		
		$numbers[] = $hundred . ' hundred';
		
		foreach ( $ones as $one ) {
			$numbers[] = $hundred . ' hundred and ' . $one;
		}
		
		foreach ( $teens as $teen ) {
			$numbers[] = $hundred . ' hundred and ' . $teen;
		}
		
		foreach ( $tens as $ten ) {
			$numbers[] = $hundred . ' hundred and ' . $ten;
			
			foreach ( $ones as $one ) {
				$numbers[] = $hundred . ' hundred and ' . $ten . '-' . $one;
			}
		}
		
	}
	
	$numbers[] = 'one thousand';
	
	$letters = 0;
	foreach ( $numbers as $number ) {
		$stripped = str_replace( array( ' ', '-' ), array( '', '' ), $number );
		$letters = $letters + strlen( $stripped );
	}
	
	var_dump( $letters );
	
?>