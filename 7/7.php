<?php

	require('common.php');

	$primes = array();
	$i = 2;		// 1 is not prime?
	do {

		if ( is_prime( $i ) ) {
			$primes[] = $i;
		}

		$i++;

	}
	while ( count( $primes ) < 10001 );

	print_r($primes);

?>