<?php

	$longest_starting = 0;
	$longest_chain = 0;
	for ( $i = 1000000; $i > 1; $i-- ) {

		$chain = collatz_chain( $i );

		if ( count( $chain ) > $longest_chain ) {
			$longest_starting = $i;
			$longest_chain = count( $chain );
		}

		if ( $i % 1000 == 0 ) {
			echo $i . ': ' . count( $chain ) . "\n";
		}

	}

	echo 'Longest chain was ' . $longest_chain . ' long: ' . $longest_starting . "\n";

	function collatz_chain ( $i ) {

		$chain = array( $i );
		do {

			$i = collatz_next( $i );

			$chain[] = $i;

		}
		while ( $i > 1 );

		return $chain;

	}

	function collatz_next ( $i ) {

		if ( $i % 2 == 0 ) {
			// if even, n -> n/2
			return $i / 2;
		}
		else {
			// if odd, n -> 3n + 1
			return ( $i * 3 ) + 1;
		}

	}

?>