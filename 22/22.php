<?php

	$names = file_get_csv( 'names.txt' );

	sort( $names );

	$scores = array();
	foreach ( $names as $i => $name ) {

		$letters = str_split( $name );

		$chars = array();
		foreach ( $letters as $letter ) {
			$chars[] = ( ord( $letter ) - ord( 'A' ) ) + 1;
		}

		$scores[] = array_sum( $chars ) * ( $i + 1 );

	}

	var_dump( array_sum( $scores ) );

	function file_get_csv ( $filename ) {

		$h = fopen( $filename, 'r' );

		$data = fgetcsv( $h );

		fclose( $h );

		return $data;

	}

?>