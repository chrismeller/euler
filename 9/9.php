<?php

	$goal = 1000;

	for ( $a = 1; $a <= $goal; $a++ ) {

		for ( $b = 1; $b <= $goal; $b++ ) {

			for ( $c = 1; $c <= $goal; $c++ ) {

				// check if they equal 1000 first, it's quicker
				if ( $a + $b + $c == 1000 ) {

					if ( bcpow( $a, 2 ) + bcpow( $b, 2 ) == bcpow( $c, 2 ) ) {

						$product = bcmul( bcmul( $a, $b ), $c );

						echo $a . '^2 + ' . $b . '^2' . ' = ' . $c . '^2' . "\n";
						echo $a . ' + ' . $b . ' + ' . $c . ' = ' . $goal . "\n";
						echo 'Product abc: ' . $product . "\n";

						break 3;

					}

				}
			}

		}

	}

?>