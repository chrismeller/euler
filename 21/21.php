<?php

	require('common.php');

	$under = 10000;

	echo 'Checking 1 - ' . ( $under / 2 ) . ' and ' . ( $under / 2 ) . ' - ' . $under . "\n";

	$amicables = array();
	for ( $a = 1; $a <= $under; $a++ ) {

		// first, get the sum of the factors of a
		$d_a = factor_sum( $a );

		// now if these are amicable, d(a) should be our b... rename it
		$b = $d_a;

		// if b is equal to a we can stop right now
		if ( $b == $a ) {
			continue;
		}

		// otherwise, we need to calculate d(b) too
		$d_b = factor_sum( $b );

		// now if a = d(b) and b = d(a), we're good
		if ( $a == $d_b && $b == $d_a ) {
			echo $a . ' and ' . $b . ' are amicable!' . "\n";
			$amicables[] = $a;
		}

	}

	echo 'Found ' . count( $amicables ) . ' amicable numbers.' . "\n";
	echo 'Sum: ' . array_sum( $amicables ) . "\n";

	function factor_sum ( $i ) {

		$factors = factor( $i );

		$keyed = array_combine( $factors, $factors );

		unset( $keyed[ $i ] );

		return array_sum( $keyed );

	}
	
?>