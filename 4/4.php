<?php

	$digits = 3;

	$min = pow( 10, $digits - 1 );
	$max = pow( 10, $digits ) - 1;

	echo 'Looking for ' . $digits . ' digit numbers [' . $min . ' - ' . $max . ']' . "\n";

	$largest = 0;
	$largest_i = 0;
	$largest_j = 0;
	for ( $i = $min; $i <= $max; $i++ ) {

		for ( $j = $min; $j <= $max; $j++ ) {

			$product = $i * $j;

			if ( strrev( $product ) == $product && $product > $largest ) {
				$largest = $product;
				$largest_i = $i;
				$largest_j = $j;
			}

		}

	}

	echo $largest_i . ' x ' . $largest_j . ' = ' . $largest . "\n";

?>