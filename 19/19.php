<?php
	
	date_default_timezone_set('America/New_York');
	ini_set('display_errors', true);
	error_reporting(-1);
	
	require('common.php');
	
	$start = new DateTime('1901-01-01');
	$end = new DateTime('2000-12-31');
	
	$current = $start;
	$interval = new DateInterval( 'P1D' );
	$sundays = array();
	do {
		
		echo $current->format( DateTime::RFC822 );
		
		if ( $current->format( 'd' ) == 1 && $current->format( 'D' ) == 'Sun' ) {
			$sundays[] = $current->format( DateTime::RFC822 );
			
			echo ' MATCHED!';
		}
		
		echo "\n";
		
		$current = $current->add( $interval );
		
	}
	while ( $current <= $end );
	
	var_dump( count( $sundays ) );
	
?>