<?php

	require('common.php');

	$below = 2000000;

	$primes = array();
	for ( $i = 2; $i < $below; $i++ ) {

		if ( is_prime( $i ) ) {
			$primes[] = $i;
		}

	}

	echo 'Sum of primes: ' . array_sum( $primes ) . "\n";

?>